#include <stdio.h>

/*
 * Array fundamentals.
 */

int main(int argc, char **argv) {

  int ia[5] = {1, 2, 3, 4, 5};

  for (int i = 0; ia[i]; ++i) {
    printf("The index value is: %i\n", ia[i]);
  }

  return 0;
}

